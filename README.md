# golang-code-katas

This repository contains a selection of katas that are prepared so that you can directly dig into implementing them. They will all come with a selection of tests which show you if your solution is working or if you forgot something.

# Running
Tests are run with
```bash
go test
```
in the project directory.

For example you may run the `Supermarket Pricing` Kata with
```bash
go get gitlab.com/svenfinke/golang-code-katas/go/kata01-supermarket-pricing
cd $GOPATH/src/gitlab.com/svenfinke/golang-code-katas/go/kata01-supermarket-pricing
go test
```

# Credits
The strucutre of the repository and the idea was inspired by [go-td-katas](https://github.com/mriehl/go-tdd-katas).
Most of the Katas are based on posts on [codekata.com](http://codekata.com/).