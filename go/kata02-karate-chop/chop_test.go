package kata02_karate_chop

import "testing"

func TestChop(t *testing.T) {
	type args struct {
		needle          int
		sorted_haystack []int
	}

	singleItemArray := []int{1}
	smallArray := []int{1,3,5}
	bigArray := []int{1,3,5,7}

	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "Empty array",
			args: args{
				needle:          3,
				sorted_haystack: []int{},
			},
			want: -1,
		},{
			name: "Needle not found",
			args: args{
				needle:          3,
				sorted_haystack: singleItemArray,
			},
			want: -1,
		},{
			name: "Single Item Array",
			args: args{
				needle:          1,
				sorted_haystack: singleItemArray,
			},
			want: 0,
		},

		{
			name: "Small: First Item",
			args: args{
				needle:          1,
				sorted_haystack: smallArray,
			},
			want: 0,
		},{
			name: "Small: Second Item",
			args: args{
				needle:          3,
				sorted_haystack: smallArray,
			},
			want: 1,
		},{
			name: "Small: Third Item",
			args: args{
				needle:          5,
				sorted_haystack: smallArray,
			},
			want: 2,
		},{
			name: "Small: Item not present",
			args: args{
				needle:          0,
				sorted_haystack: smallArray,
			},
			want: -1,
		},{
			name: "Small: Item not present",
			args: args{
				needle:          2,
				sorted_haystack: smallArray,
			},
			want: -1,
		},{
			name: "Small: Item not present",
			args: args{
				needle:          4,
				sorted_haystack: smallArray,
			},
			want: -1,
		},{
			name: "Small: Item not present",
			args: args{
				needle:          6,
				sorted_haystack: smallArray,
			},
			want: -1,
		},


		{
			name: "Big: First Item",
			args: args{
				needle:          1,
				sorted_haystack: bigArray,
			},
			want: 0,
		},{
			name: "Big: Second Item",
			args: args{
				needle:          3,
				sorted_haystack: bigArray,
			},
			want: 1,
		},{
			name: "Big: Third Item",
			args: args{
				needle:          5,
				sorted_haystack: bigArray,
			},
			want: 2,
		},{
			name: "Big: Fourth Item",
			args: args{
				needle:          7,
				sorted_haystack: bigArray,
			},
			want: 3,
		},{
			name: "Big: Not Found",
			args: args{
				needle:          0,
				sorted_haystack: bigArray,
			},
			want: -1,
		},{
			name: "Big: Not Found",
			args: args{
				needle:          2,
				sorted_haystack: bigArray,
			},
			want: -1,
		},{
			name: "Big: Not Found",
			args: args{
				needle:          4,
				sorted_haystack: bigArray,
			},
			want: -1,
		},{
			name: "Big: Not Found",
			args: args{
				needle:          6,
				sorted_haystack: bigArray,
			},
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Chop(tt.args.needle, tt.args.sorted_haystack); got != tt.want {
				t.Errorf("Chop() = %v, want %v", got, tt.want)
			}
		})
	}
}